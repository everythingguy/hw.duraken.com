var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var browserSync = require('browser-sync').create();

gulp.task('gulp_nodemon', function () {
    nodemon({
        script: 'app.js', //this is where my express server is
            
        ext: 'js', //nodemon watches *.js

        ignore: ['public', 'Gulpfile.js'], //except js files in the public/js dir since these are run client side not server side
            
        env: {
            'NODE_ENV': 'development',
            'PORT': 8080
        }
    });
});

gulp.task('sync', function () {
    browserSync.init({
        port: 80, //this can be any port, it will show our app
        proxy: {
            target: "http://localhost:8080/", //this is the port where express server works
            reqHeaders: (config) => {
                return {
                    "host": process.argv[4], //domain name to pass to node app
                    "accept-encoding": "identity",
                    "agent": false
                }
            }
        },
        ui: {
            port: 81
        }, //UI, can be any port
        reloadDelay: 3000, //Important, otherwise syncing will not work
        open: false
    });
    gulp.watch(['./**/*.js', './**/*.html', './**/*.css', './**/*.hbs']).on("change",
        browserSync.reload); //reloads web browser on port 3002 if a js, html, css, or hbs is saved
});

gulp.task('default', gulp.parallel('gulp_nodemon', 'sync'));