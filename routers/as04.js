const express = require("express");
const fs = require("fs");

const router = express.Router();

router.get("/home", (req, res) => {
    res.sendFile(__dirname.replace("routers", "") + "/public/hw/quiz/as04.html");
});

router.get("/about", (req, res) => {
    
});

router.get("/products", (req, res) => {
    
});

router.get("/services", (req, res) => {
    
});

router.get("/contact", (req, res) => {
    
});

router.get("/quiz", (req, res) => {
    res.sendFile(__dirname.replace("routers", "") + "/public/hw/quiz/index.html");
});

router.get("/quiz/results", (req, res) => {
    res.sendFile(__dirname.replace("routers", "") + "/public/hw/quiz/results.html");
});

router.get("/quiz/chart", (req, res) => {
    res.sendFile(__dirname.replace("routers", "") + "/public/hw/images/Quiz.pdf");
});

router.get("/quiz/data/questions", (req, res) => {
    res.json(JSON.parse(fs.readFileSync(__dirname.replace("routers", "") + "/public/hw/quiz/questions.json")));
});

router.get("/", (req, res) => {
    res.render("as04", {
        layout: "blank.hbs"
    });
});

module.exports = router;