const express = require("express");

const router = express.Router();

router.get("/home", (req, res) => {
    res.sendFile(__dirname.replace("routers", "") + "/public/hw/chartjs/index.html");
});

module.exports = router;