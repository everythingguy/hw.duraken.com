const express = require("express");

const as01Router = require("./stage")(1);
const as02Router = require("./stage")(2);
const as03Router = require("./bootstrap");
const as04Router = require("./as04");
const as05Router = require("./as05");
const as06Router = require("./as06");
const chartjsRouter = require("./chartjs");
const blackjackRouter = require("./blackjack");

const router = express.Router();
const env = process.env.NODE_ENV || 'dev';

router.get("*", (req, res, next) => {
    if (req.headers.host == "hw.duraken.com") {
        next();
    } else if (env == "dev") {
        next();
    } else {
        res.sendStatus(404);
    }
});

router.get("/", (req, res) => {
    res.redirect("/cis255.html");
});

router.get("/cis255.html", (req, res) => {
    res.render("cis255", {
        layout: "blank.hbs"
    });
});

router.use("/as01", as01Router);
router.use("/as02", as02Router);
router.use("/as03", as03Router);
router.use("/as04", as04Router);
router.use("/as05", as05Router);
router.use("/as06", as06Router);
router.use("/chartjs", chartjsRouter);
router.use("/blackjack", blackjackRouter);

module.exports = router;