const express = require("express");

const router = express.Router();

router.get("/", (req, res) => {
    res.send("Site under construction please come back later");
});

module.exports = router;