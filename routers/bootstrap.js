const express = require("express");

const router = express.Router();

router.get("/home", (req, res) => {
    res.sendFile(__dirname.replace("routers", "") + "/public/hw/bootstrap/main/index.html");
});

router.get("/about", (req, res) => {
    res.sendFile(__dirname.replace("routers", "") + "/public/hw/bootstrap/about.html");
});

router.get("/products", (req, res) => {
    res.sendFile(__dirname.replace("routers", "") + "/public/hw/bootstrap/store/index.html");
});

router.get("/services", (req, res) => {
    res.sendFile(__dirname.replace("routers", "") + "/public/hw/bootstrap/store/services.html");
});

router.get("/contact", (req, res) => {
    res.sendFile(__dirname.replace("routers", "") + "/public/hw/bootstrap/contact.html");
});

router.get("/", (req, res) => {
    res.redirect("/as03/home");
});

module.exports = router;