const express = require("express");

module.exports = (stage) => {
    const router = express.Router();

    router.get("/home", (req, res) => {
        res.render("home", {
            title: "Home",
            styles: ["/css/main.css"],
            scripts: ["/js/main.js"],
            stage: stage
        });
    });
    
    router.get("/about", (req, res) => {
        res.render("about", {
            title: "About",
            styles: ["/css/main.css"],
            scripts: ["/js/main.js"],
            stage: stage
        });
    });
    
    router.get("/products", (req, res) => {
        res.render("products", {
            title: "Products",
            styles: ["/css/main.css"],
            scripts: ["/js/main.js"],
            stage: stage
        });
    });
    
    router.get("/services", (req, res) => {
        res.render("services", {
            title: "Services",
            styles: ["/css/main.css"],
            scripts: ["/js/main.js"],
            stage: stage
        });
    });
    
    router.get("/contact", (req, res) => {
        res.render("contact", {
            title: "Contact",
            styles: ["/css/main.css"],
            scripts: ["/js/main.js"],
            stage: stage
        });
    });

    return router;
}