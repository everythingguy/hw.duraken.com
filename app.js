const express = require("express");
const path = require("path");
const hbs = require("express-handlebars");
const Handlebars = require("handlebars");

const app = express();

app.engine(".hbs", hbs({ 
    defaultLayout: 'main', 
    extname: '.hbs', 
    layoutsDir: 'views/layouts', 
    partialsDir: 'views/partials' 
}));
app.set("view engine", ".hbs");

Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
    switch (operator) {
        case '==':
            return (v1 == v2) ? options.fn(this) : options.inverse(this);
        case '===':
            return (v1 === v2) ? options.fn(this) : options.inverse(this);
        case '!=':
            return (v1 != v2) ? options.fn(this) : options.inverse(this);
        case '!==':
            return (v1 !== v2) ? options.fn(this) : options.inverse(this);
        case '<':
            return (v1 < v2) ? options.fn(this) : options.inverse(this);
        case '<=':
            return (v1 <= v2) ? options.fn(this) : options.inverse(this);
        case '>':
            return (v1 > v2) ? options.fn(this) : options.inverse(this);
        case '>=':
            return (v1 >= v2) ? options.fn(this) : options.inverse(this);
        case '&&':
            return (v1 && v2) ? options.fn(this) : options.inverse(this);
        case '||':
            return (v1 || v2) ? options.fn(this) : options.inverse(this);
        default:
            return options.inverse(this);
    }
});

Handlebars.registerHelper("math", function(lvalue, operator, rvalue, options) {
    lvalue = parseFloat(lvalue);
    rvalue = parseFloat(rvalue);
        
    return {
        "+": lvalue + rvalue,
        "-": lvalue - rvalue,
        "*": lvalue * rvalue,
        "/": lvalue / rvalue,
        "%": lvalue % rvalue
    }[operator];
});

Handlebars.registerHelper('for', function(n, block) {
    var accum = '';
    for(var i = 0; i < n; ++i)
        accum += block.fn(i);
    return accum;
});

const mainRouter = require("./routers/main");
const hwRouter = require("./routers/hw");

app.get('*', (req, res, next) => {
    if(req.headers.host == "hw.duraken.com") {
        req.url = "/hw" + req.url;
    }
    next();
});

app.use("/", express.static(path.join(__dirname, "public/main")));
app.use("/hw", express.static(path.join(__dirname, "public/hw")));
app.use(express.json());

app.use("/", mainRouter);
app.use("/hw", hwRouter);

const port = process.env.PORT || 80;
app.set('port', port);

app.listen(port, ()=> {
    console.log(`Server listening on port ${port}`);
});